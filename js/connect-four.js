const columns = 7;
const rows = 6;
document.documentElement.style.setProperty('--current-disc', 'red');


//INIT
for (let i = 0; i < rows+1; i++) {
    var row = document.createElement('div');
    row.classList.add('row');

    //Create columns
    for (let j = 0; j < columns; j++) {
        var column = document.createElement('div');
        column.classList.add('column');

        //Add click event for first row
        if (i === 0)
            column.addEventListener('click', onClick)

        row.appendChild(column);
    }

    document.getElementById("board-game").append(row);
}

function onClick (e) {
    var allColumns = Array.from(document.getElementsByClassName("column"));
    allColumns.splice(0, columns);

    let root = document.documentElement;
    let current = e.currentTarget;
    let target = getLastUnoccupied(current);
    if (target === false)
        return; //Invaild play

    //Animation:
    current.style.setProperty('background-color', 'var(--current-disc)');
    current.style.setProperty('transform', `translateY(${14*Math.ceil((target+1)/columns)}${document.getElementsByTagName('body')[0].clientWidth > 820 ? 'vh' : 'vw'})`);
    
    var transitionEnd = whichTransitionEvent();
    current.addEventListener(transitionEnd, afterFallAnimation, false);
    /* END OF STREAM */

    function afterFallAnimation() {
        current.removeEventListener(transitionEnd, afterFallAnimation);
        allColumns[target].style.setProperty('background-color', root.style.getPropertyValue('--current-disc'));
        allColumns[target].classList.add('stamp');


        root.style.setProperty('--current-disc', root.style.getPropertyValue('--current-disc') == 'lightblue' ? 'red' : 'lightblue');
        
        //Reset:
            current.style.setProperty('background-color', 'transparent');
            current.style.setProperty('transition', 'none');
            current.style.setProperty('transform', `translateY(0)`);
            setTimeout(function(){  
                current.style.setProperty('transition', 'transform 0.4s linear');    
            }, 20);
            //Turn Label:
            document.getElementById('turn-label').textContent = root.style.getPropertyValue('--current-disc') == 'red' ? "Reds Turn!" : "Blue's Turn!";
            document.getElementById('turn-label').style.setProperty('color', root.style.getPropertyValue('--current-disc') == 'red' ? "red" : "lightblue");


        /* Check Win Conditions */
        let colorWon = checkWin();
        if (colorWon !== false) {
            alert(`${colorWon == 'red' ? 'Red' : 'Blue'} Win's!`);
            Array.from(document.getElementsByClassName("column")).slice(0, columns).forEach(x=>x.removeEventListener('click', onClick));
        }
        //Tie??
        let flag = true;
        for (let i = 0; i < columns; i++)
            if (allColumns[i].style.getPropertyValue('background-color') === '')
                flag = false;
        if (flag) {
            alert(`It's A Tie!`);
            Array.from(document.getElementsByClassName("column")).slice(0, columns).forEach(x=>x.removeEventListener('click', onClick));
        }

    }
}

function getLastUnoccupied (current) {
    var allColumns = Array.from(document.getElementsByClassName("column"));

    //Which Column?
    let index = 0;
    allColumns.forEach((x,i)=>{if (current === x) index = i;});
    allColumns.splice(0, columns);

    //Check if column isn't full
    if (allColumns[index].style.getPropertyValue('background-color') != '')
        return false;

    //Which Row?
    for (let i = index; i < allColumns.length; i=i+columns) {
        if (allColumns[i].style.getPropertyValue('background-color') != '')
            return i-columns;
    }
    return ((columns*rows) - columns) + index;
}

function checkWin () {
    var allColumns = Array.from(document.getElementsByClassName("column"));
    allColumns.splice(0, columns);
    var matrix = [];
    while(allColumns.length) matrix.push(allColumns.splice(0,columns));


    //Horizontal
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length-3; j++) {
            let flag = true;

            for (let n = 1; n < 4; n++) {
                if (matrix[i][j].style.getPropertyValue('background-color') == '' || matrix[i][j].style.getPropertyValue('background-color') != matrix[i][j+n].style.getPropertyValue('background-color'))
                    flag = false;
            }
            if (flag)
                return matrix[i][j].style.getPropertyValue('background-color');
        }
    }
    //Vertical
    for (let i = 0; i < matrix.length-3; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            let flag = true;

            for (let n = 1; n < 4; n++) {
                if (matrix[i][j].style.getPropertyValue('background-color') == '' || matrix[i][j].style.getPropertyValue('background-color') != matrix[i+n][j].style.getPropertyValue('background-color'))
                    flag = false;
            }
            if (flag)
                return matrix[i][j].style.getPropertyValue('background-color');
        }
    }

    //Cross Positive
    for (let i = 0; i < matrix.length-3; i++) {
        for (let j = 0; j < matrix[i].length-3; j++) {
            let flag = true;

            for (let n = 1; n < 4; n++) {
                if (matrix[i][j].style.getPropertyValue('background-color') == '' || matrix[i][j].style.getPropertyValue('background-color') != matrix[i+n][j+n].style.getPropertyValue('background-color'))
                    flag = false;
            }
            if (flag)
                return matrix[i][j].style.getPropertyValue('background-color');
        }
    }

    //Cross Negative
    for (let i = 0; i < matrix.length-3; i++) {
        for (let j = matrix.length; j > 2; j--) {
            let flag = true;

            for (let n = 1; n < 4; n++) {
                if (matrix[i][j].style.getPropertyValue('background-color') == '' || matrix[i][j].style.getPropertyValue('background-color') != matrix[i+n][j-n].style.getPropertyValue('background-color'))
                    flag = false;
            }
            if (flag)
                return matrix[i][j].style.getPropertyValue('background-color');
        }
    }

    //Nothing Found:
    return false;
}

function whichTransitionEvent() {
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
      'transition':'transitionend',
      'OTransition':'oTransitionEnd',
      'MozTransition':'transitionend',
      'WebkitTransition':'webkitTransitionEnd'
    }

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
}